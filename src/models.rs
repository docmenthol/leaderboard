use crate::schema::*;
use chrono::NaiveDateTime;
use diesel::backend::Backend;
use diesel::deserialize::FromSql;
use diesel::serialize::{Output, ToSql};
use diesel::sql_types::Integer;
use diesel::{deserialize, serialize};
use serde::{Deserialize, Serialize};
use std::io::Write;

#[derive(Debug, Clone, AsExpression, Queryable)]
pub struct UserDTO {
    pub id: i32,
    pub username: String,
    pub password: String,
    pub discriminator: String,
    pub join_date: NaiveDateTime,
    pub api_key: Option<String>,
    pub access: AccessLevel,
    pub xp: i32,
}

#[derive(Serialize, Debug, Clone, AsExpression, Queryable)]
pub struct GameDTO {
    pub id: i32,
    pub name: String,
}

#[derive(Serialize, Debug, Clone, AsExpression, Queryable)]
pub struct ModeDTO {
    id: i32,
    game_id: i32,
    name: String,
    units: String,
    prefix: bool,
    adjacent: bool,
}

#[derive(Serialize, Debug, Clone, AsExpression, Queryable)]
pub struct ScoreDTO {
    id: i32,
    user_id: i32,
    mode_id: i32,
    score: f32,
}

#[derive(Debug, Clone, AsExpression, Queryable)]
pub struct SessionDTO {
    id: i32,
    pub token: String,
    user_id: i32,
    expires: NaiveDateTime,
}

#[derive(Debug, Clone, Insertable)]
#[table_name = "users"]
pub struct CreateUserDTO {
    pub username: String,
    pub password: String,
    pub discriminator: String,
    pub access: AccessLevel,
}

#[derive(Deserialize, Debug, Clone, Insertable)]
#[table_name = "games"]
pub struct CreateGameDTO {
    pub name: String,
}

#[derive(Deserialize, Debug, Clone, Insertable)]
#[table_name = "modes"]
pub struct CreateModeDTO {
    game_id: i32,
    name: String,
    units: String,
    prefix: bool,
    adjacent: bool,
}

#[derive(Debug, Clone, Insertable)]
#[table_name = "sessions"]
pub struct CreateSessionDTO {
    pub user_id: i32,
    pub expires: NaiveDateTime,
}

#[derive(Deserialize, Serialize, Debug, Clone, Copy, AsExpression, FromSqlRow)]
#[sql_type = "Integer"]
pub enum AccessLevel {
    Anonymous,
    Registered,
    Player,
    Administrator = 1000,
}

impl UserDTO {
    #[allow(dead_code)]
    pub fn display_name(&self) -> String {
        format!("{}#{}", self.username, self.discriminator)
    }
}

impl<DB> ToSql<Integer, DB> for AccessLevel
where
    DB: Backend,
    i32: ToSql<Integer, DB>,
{
    fn to_sql<W: Write>(&self, out: &mut Output<W, DB>) -> serialize::Result {
        (*self as i32).to_sql(out)
    }
}

impl<DB> FromSql<Integer, DB> for AccessLevel
where
    DB: Backend,
    i32: FromSql<Integer, DB>,
{
    fn from_sql(bytes: Option<&DB::RawValue>) -> deserialize::Result<Self> {
        match i32::from_sql(bytes)? {
            1 => Ok(AccessLevel::Registered),
            2 => Ok(AccessLevel::Player),
            1000 => Ok(AccessLevel::Administrator),
            _ => Ok(AccessLevel::Anonymous),
        }
    }
}
