table! {
    games (id) {
        id -> Int4,
        name -> Text,
    }
}

table! {
    modes (id) {
        id -> Int4,
        game_id -> Int4,
        name -> Text,
        units -> Text,
        prefix -> Bool,
        adjacent -> Bool,
    }
}

table! {
    scores (id) {
        id -> Int4,
        player_id -> Int4,
        mode_id -> Int4,
        score -> Float4,
    }
}

table! {
    sessions (id) {
        id -> Int4,
        token -> Text,
        user_id -> Int4,
        expires -> Timestamp,
    }
}

table! {
    users (id) {
        id -> Int4,
        username -> Text,
        password -> Text,
        discriminator -> Varchar,
        join_date -> Timestamp,
        api_key -> Nullable<Text>,
        access -> Int4,
        xp -> Int4,
    }
}

allow_tables_to_appear_in_same_query!(
    games,
    modes,
    scores,
    sessions,
    users,
);
