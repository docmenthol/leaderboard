use crate::data_access::DBAccessManager;
use crate::errors::{AppError, ErrorType};
use crate::models::{AccessLevel, UserDTO};
use log;
use serde::{Deserialize, Serialize};
use warp::http::StatusCode;
use warp::{Rejection, Reply};

#[derive(Deserialize, Debug)]
pub struct Login {
    pub username: String,
    pub password: String,
}

#[derive(Deserialize, Debug)]
pub struct Register {
    pub username: String,
    pub password: String,
    pub password_again: String,
    pub email: String,
}

#[derive(Serialize, Debug)]
pub struct User {
    pub id: i32,
    pub username: String,
    pub join_date: String,
    pub access: AccessLevel,
    pub xp: i32,
}

#[derive(Debug)]
pub struct Token {
    pub token: String,
}

impl User {
    #[allow(dead_code)]
    pub fn from_dto(u: UserDTO) -> User {
        User {
            id: u.id.clone(),
            username: u.username.clone(),
            join_date: "".to_string(),
            access: u.access.clone(),
            xp: u.xp.clone(),
        }
    }
}

fn respond<T: Serialize>(
    result: Result<T, AppError>,
    status: warp::http::StatusCode,
) -> Result<impl Reply, Rejection> {
    match result {
        Ok(response) => Ok(warp::reply::with_status(
            warp::reply::json(&response),
            status,
        )),
        Err(err) => {
            log::error!("Error while trying to respond: {}", err.to_string());
            Err(warp::reject::custom(err))
        }
    }
}

pub async fn cookie_check(cookie: Option<String>) -> Result<impl Reply, Rejection> {
    let resp = match cookie {
        Some(c) => Ok(format!("cookie good: {}", c)),
        None => Err(AppError::new("No cookie.", ErrorType::Unauthorized)),
    };

    respond(resp, StatusCode::OK)
}

pub async fn login(creds: Login, dbam: DBAccessManager) -> Result<impl Reply, Rejection> {
    let login_result = dbam.login(creds);

    // TODO: Handle result and then either send response with
    // cookie or send a login error response.

    respond(login_result, StatusCode::OK)
}

pub async fn register(
    registration: Register,
    dbam: DBAccessManager,
) -> Result<impl Reply, Rejection> {
    respond(dbam.register(registration), StatusCode::OK)
}

pub async fn logout(_dbam: DBAccessManager) -> Result<impl Reply, Rejection> {
    // TODO: Delete cookie and session.
    Ok("logout")
}

pub async fn list_games(dbam: DBAccessManager) -> Result<impl Reply, Rejection> {
    respond(dbam.list_games(), StatusCode::OK)
}

pub async fn list_modes_for_game(
    game_id: i32,
    dbam: DBAccessManager,
) -> Result<impl Reply, Rejection> {
    respond(dbam.list_modes_for_game(game_id), StatusCode::OK)
}

pub async fn list_scores_for_mode(
    mode_id: i32,
    dbam: DBAccessManager,
) -> Result<impl Reply, Rejection> {
    respond(dbam.list_scores_for_mode(mode_id), StatusCode::OK)
}
