use crate::api;
use crate::db::{with_db_access_manager, PgPool};
use warp::{Filter, Rejection, Reply};

pub fn login(pool: PgPool) -> impl Filter<Extract = impl Reply, Error = Rejection> + Clone {
    warp::path!("login")
        .and(warp::post())
        .and(warp::body::json())
        .and(with_db_access_manager(pool))
        .and_then(api::login)
}

pub fn register(pool: PgPool) -> impl Filter<Extract = impl Reply, Error = Rejection> + Clone {
    warp::path!("register")
        .and(warp::post())
        .and(warp::body::json())
        .and(with_db_access_manager(pool))
        .and_then(api::register)
}

pub fn logout(pool: PgPool) -> impl Filter<Extract = impl Reply, Error = Rejection> + Clone {
    warp::path!("logout")
        .and(warp::get())
        .and(with_db_access_manager(pool))
        .and_then(api::logout)
}

pub fn get_games(pool: PgPool) -> impl Filter<Extract = impl Reply, Error = Rejection> + Clone {
    warp::path!("games")
        .and(warp::get())
        .and(with_db_access_manager(pool))
        .and_then(api::list_games)
}

pub fn get_modes(pool: PgPool) -> impl Filter<Extract = impl Reply, Error = Rejection> + Clone {
    warp::path!("modes" / i32)
        .and(warp::get())
        .and(with_db_access_manager(pool))
        .and_then(api::list_modes_for_game)
}

pub fn get_scores(pool: PgPool) -> impl Filter<Extract = impl Reply, Error = Rejection> + Clone {
    warp::path!("scores" / i32)
        .and(warp::get())
        .and(with_db_access_manager(pool))
        .and_then(api::list_scores_for_mode)
}

pub fn cookie_check() -> impl Filter<Extract = impl Reply, Error = Rejection> + Clone {
    warp::path!("cookie")
        .and(warp::get())
        .and(warp::filters::cookie::optional("leaderboards"))
        .and_then(api::cookie_check)
}

pub fn api_filters(pool: PgPool) -> impl Filter<Extract = impl Reply, Error = Rejection> + Clone {
    warp::path!("api" / "v2" / ..).and(
        login(pool.clone())
            .or(register(pool.clone()))
            .or(logout(pool.clone()))
            .or(get_games(pool.clone()))
            .or(get_modes(pool.clone()))
            .or(get_scores(pool.clone()))
            .or(cookie_check()),
    )
}
