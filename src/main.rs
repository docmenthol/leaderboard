#[macro_use]
extern crate diesel;

mod api;
mod data_access;
mod db;
mod errors;
mod hash;
mod models;
mod routes;
mod schema;

use crate::errors::{AppError, ErrorType};
use dotenv::dotenv;
use log;
use std::env;
use warp::Filter;

#[tokio::main]
async fn main() {
    dotenv().ok();

    if env::var_os("RUST_LOG").is_none() {
        env::set_var("RUST_LOG", "info");
    }

    pretty_env_logger::init();

    log::info!(".: leaderboard api");

    let db_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set.");
    let pool = db::pg_pool(db_url.as_str());
    let api = routes::api_filters(pool).recover(errors::handle_rejection);

    warp::serve(api).run(([127, 0, 0, 1], 4000)).await;
}
