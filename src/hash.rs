use bcrypt;

#[allow(dead_code)]
pub fn hash(password: &[u8]) -> String {
    bcrypt::hash(password, 8).unwrap()
}

pub fn verify(password: &[u8], hash: &str) -> Result<bool, String> {
    match bcrypt::verify(password, hash) {
        Ok(r) => Ok(r),
        Err(err) => Err(format!("{:?}", err)),
    }
}
