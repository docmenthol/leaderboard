#![allow(dead_code)]
use crate::api::{Login, Register, Token};
use crate::errors::{AppError, ErrorType};
use crate::hash::verify;
use crate::models::{
    CreateSessionDTO, CreateUserDTO, GameDTO, ModeDTO, ScoreDTO, SessionDTO, UserDTO,
};
use chrono::naive::NaiveDateTime;
use chrono::offset::Utc;
use chrono::Duration;
use diesel::prelude::*;
use diesel::r2d2::{ConnectionManager, PooledConnection};

type PooledPg = PooledConnection<ConnectionManager<PgConnection>>;

pub struct DBAccessManager {
    connection: PooledPg,
}

impl DBAccessManager {
    pub fn new(connection: PooledPg) -> DBAccessManager {
        DBAccessManager { connection }
    }

    pub fn create_user(&self, dto: CreateUserDTO) -> Result<UserDTO, AppError> {
        use super::schema::users;

        diesel::insert_into(users::table)
            .values(&dto)
            .get_result(&self.connection)
            .map_err(|err| AppError::from_diesel_err(err, "while creating user"))
    }

    pub fn delete_user(&self, user_id: i32) -> Result<usize, AppError> {
        use super::schema::users::dsl::*;

        let deleted = diesel::delete(users.filter(id.eq(user_id)))
            .execute(&self.connection)
            .map_err(|err| AppError::from_diesel_err(err, "while deleting user"))?;

        if deleted == 0 {
            return Err(AppError::new("User not found", ErrorType::NotFound));
        }

        Ok(deleted)
    }

    pub fn create_session(&self, uid: i32) -> Result<SessionDTO, AppError> {
        use super::schema::sessions;
        use super::schema::sessions::dsl::*;

        let dur = Utc::now() + Duration::days(7);
        let date_expires = dur.date().naive_utc();
        let time_expires = dur.time();

        let expiration_dt = NaiveDateTime::new(date_expires, time_expires);

        diesel::delete(sessions.filter(user_id.eq(uid)))
            .execute(&self.connection)
            .map_err(|err| AppError::from_diesel_err(err, "while deleting session"))?;

        let dto = CreateSessionDTO {
            user_id: uid,
            expires: expiration_dt,
        };

        diesel::insert_into(sessions::table)
            .values(&dto)
            .get_result(&self.connection)
            .map_err(|err| AppError::from_diesel_err(err, "while creating session"))
    }

    pub fn get_session(&self, uid: i32) -> Result<Token, AppError> {
        use super::schema::sessions::dsl::*;

        let utc_now = Utc::now();
        let date_now = utc_now.date().naive_utc();
        let time_now = utc_now.time();

        let now = NaiveDateTime::new(date_now, time_now);

        let existing: Result<SessionDTO, diesel::result::Error> = sessions
            .filter(user_id.eq(uid))
            .filter(expires.lt(now))
            .get_result(&self.connection);

        match existing {
            Ok(session) => Ok(Token {
                token: session.token,
            }),
            Err(_) => match self.create_session(uid) {
                Ok(session) => Ok(Token {
                    token: session.token,
                }),
                Err(e) => Err(e),
            },
        }
    }

    pub fn login(&self, creds: Login) -> Result<bool, AppError> {
        use super::schema::users::dsl::*;

        let user: UserDTO = match users
            .filter(username.eq(creds.username))
            .get_result(&self.connection)
        {
            Ok(u) => u,
            Err(_) => {
                return Err(AppError::new(
                    "Invalid username/password.",
                    ErrorType::Unauthorized,
                ))
            }
        };

        match verify(user.password.as_bytes(), &creds.password) {
            Ok(b) => Ok(b),
            Err(_) => Err(AppError::new(
                "Invalid username/password.",
                ErrorType::Unauthorized,
            )),
        }
    }

    pub fn register(&self, _registration: Register) -> Result<(), AppError> {
        Err(AppError::new("Not implemented.", ErrorType::Internal))
    }

    pub fn list_games(&self) -> Result<Vec<GameDTO>, AppError> {
        use super::schema::games::dsl::*;

        games
            .load(&self.connection)
            .map_err(|err| AppError::from_diesel_err(err, "while listing games"))
    }

    pub fn list_modes_for_game(&self, gid: i32) -> Result<Vec<ModeDTO>, AppError> {
        use super::schema::modes::dsl::*;

        modes
            .filter(game_id.eq(gid))
            .load(&self.connection)
            .map_err(|err| {
                AppError::from_diesel_err(
                    err,
                    format!("while listing modes for game_id = {}", gid).as_str(),
                )
            })
    }

    pub fn list_scores_for_mode(&self, mid: i32) -> Result<Vec<ScoreDTO>, AppError> {
        use super::schema::scores::dsl::*;

        scores
            .filter(mode_id.eq(mid))
            .load(&self.connection)
            .map_err(|err| {
                AppError::from_diesel_err(
                    err,
                    format!("while listing scores for mode_id = {}", mid).as_str(),
                )
            })
    }
}
