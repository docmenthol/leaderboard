CREATE TABLE users (
  id            SERIAL PRIMARY KEY NOT NULL,
  username      TEXT NOT NULL,
  password      TEXT NOT NULL,
  discriminator VARCHAR(4) NOT NULL,
  join_date     TIMESTAMP NOT NULL DEFAULT NOW(),
  api_key       TEXT,
  access        INTEGER NOT NULL DEFAULT 0,
  xp            INTEGER NOT NULL DEFAULT 0
);

INSERT INTO users
  ( username, discriminator,
    password,
    access, xp
  )
VALUES ( 'admin', '0001',
         '$2b$08$iI20g89tExKycjTRgt3lcuFAmexc2MLw7qw9ZUNYq3/nP6DaipXOS',
         1000, 0 ),
       ( 'user_i', '0020',
         '$2b$08$csB5nHd0n59H5jUOVcOAJ.uHz1xE8lC2V3IXW1Su5osQYic4NldFG',
         1, 0 ),
       ( 'user_r', '0300',
         '$2b$08$rSGBWk/2yAc71JcAGZOrMeK/y2fv/EYY2TUXItS5QEjVoyZWntkQO',
         1, 0 ),
       ( 'user_j', '4000',
         '$2b$08$/YJD0yDG1oHMBrNuu25YYeCT1d4zOlQYtRGCaIuGJW/SDOYbWN4Jq',
         1, 0 ),
       ( 'user_d', '0005',
         '$2b$08$0O5jNJu6zB7QJxxzWZOgoeEFwDxfrRDKuDUZzIdk930jHkLUwX78m',
         1, 0 );