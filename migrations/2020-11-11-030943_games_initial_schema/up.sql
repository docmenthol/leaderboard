CREATE TABLE games (
  id   SERIAL PRIMARY KEY NOT NULL,
  name TEXT NOT NULL
);

INSERT INTO games ( name )
VALUES ( 'H3VR Boomskee' ),
       ( 'Xortex 26XX' ),
       ( 'Slingshot' ),
       ( 'Longbow' ),
       ( 'Gorn' ),
       ( 'Ping Pong' );