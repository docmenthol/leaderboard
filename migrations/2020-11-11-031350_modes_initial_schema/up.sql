CREATE TABLE modes (
  id       SERIAL PRIMARY KEY NOT NULL,
  game_id  INTEGER NOT NULL,
  name     TEXT NOT NULL,
  units    TEXT NOT NULL,
  prefix   BOOLEAN NOT NULL DEFAULT false,
  adjacent BOOLEAN NOT NULL DEFAULT false
);

INSERT INTO modes ( game_id, name, units, prefix, adjacent )
VALUES ( 1, 'Full Game', 'points', false, false ),
       ( 1, 'One Grenade', 'points', false, false ),
       ( 2, 'Infinite', 'points', false, false ),
       ( 3, 'Full Game', '$', true, true ),
       ( 4, 'Points', 'points', false, false ),
       ( 4, 'Wave', 'Wave', true, false ),
       ( 5, 'Endless', 'kills', false, false ),
       ( 5, 'rock%', 'kills', false, false ),
       ( 6, 'Quadrants', 'points', false, false );